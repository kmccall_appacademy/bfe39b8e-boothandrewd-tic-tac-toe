# lib/board.rb

class Board
  attr_reader :grid

  def initialize(grid = [[nil] * 3, [nil] * 3, [nil] * 3])
    @grid = grid
  end

  def place_mark(pair, mark)
    @grid[pair.first][pair.last] = mark if empty?(pair)
  end

  def empty?(pair)
    @grid.dig(*pair).nil?
  end

  def winner
    # Check rows
    rows.map(&:uniq).each do |row_uniqs|
      return row_uniqs.first if row_uniqs.count == 1 && row_uniqs.first
    end

    # Check columns
    columns.map(&:uniq).each do |col_uniqs|
      return col_uniqs.first if col_uniqs.count == 1 && col_uniqs.first
    end

    # Check diagonals
    diagonals.map(&:uniq).each do |diag_uniqs|
      return diag_uniqs.first if diag_uniqs.count == 1 && diag_uniqs.first
    end

    # Return nil if no 3-in-a-rows found
    nil
  end

  def rows
    @grid
  end

  def columns
    @grid.transpose
  end

  def diagonals
    (0..2).each_with_object([[], []]) do |index, diags|
      diags.first << @grid[index][index]
      diags.last << @grid[index][-1 - index]
    end
  end

  def over?
    winner || @grid.all? { |row| row.none?(&:nil?) }
  end

  def display
    puts(rows.map do |row|
      row.map { |mark| mark.nil? ? '-' : mark }.join(' ' * 2)
    end.join("\n" * 2))
  end
end
