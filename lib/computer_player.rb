# lib/computer_player.rb

class ComputerPlayer
  attr_accessor :mark
  attr_reader :name, :board

  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
  end

  def get_move
    # Check rows
    @board.rows.each_with_index do |row, index|
      return [index, row.index(nil)] if row.count(@mark) == 2
    end

    # Check columns
    @board.columns.each_with_index do |column, index|
      return [column.index(nil), index] if column.count(@mark) == 2
    end

    # Check negative sloping diagonal
    neg_diag = @board.diagonals.first
    return [neg_diag.index(nil)] * 2 if neg_diag.count(@mark) == 2

    # Check positive sloping diagonal
    pos_diag = @board.diagonals.last
    return [pos_diag.index(nil), -1 - pos_diag.index(nil)] if pos_diag.count(@mark) == 2

    # If all else fails, try a random guess
    loop do
      rand_pair = [rand(0..2), rand(0..2)]
      return rand_pair if @board.empty?(rand_pair)
    end
  end
end
