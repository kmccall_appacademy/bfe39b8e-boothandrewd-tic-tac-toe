# lib/game.rb
require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_reader :board

  def initialize(player_1, player_2)
    player_1.mark = :X
    player_2.mark = :O
    @players = [player_1, player_2]
    @board = Board.new
  end

  def current_player
    @players.first
  end

  def switch_players!
    @players.reverse!
  end

  def play_turn
    current_player.display(@board)
    @board.place_mark(current_player.get_move, current_player.mark)
    switch_players!
  end

  def play
    # Play game
    until @board.over?
      puts
      play_turn
    end

    # Output endgame info
    puts
    puts
    if @board.winner.nil?
      puts "It's a tie!"
    else
      puts @players.select { |player| player.mark == @board.winner }.first.name + ' wins!'
    end
    @board.display
  end
end
