# lib/human_player.rb

class HumanPlayer
  attr_accessor :mark
  attr_reader :name

  def initialize(name)
    @name = name
  end

  def display(board)
    board.display
  end

  def get_move
    print 'Where is your next move?: '
    gets.chomp.split(',').map(&:to_i)
  end
end
